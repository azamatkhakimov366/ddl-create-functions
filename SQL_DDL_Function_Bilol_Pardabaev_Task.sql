--Create View - sales_revenue_by_category_qtr:

CREATE VIEW sales_revenue_by_category_qtr AS
SELECT 
    c.name AS category_name,
    SUM(p.amount) AS total_sales_revenue
FROM 
    category c
JOIN 
    film_category fc ON c.category_id = fc.category_id
JOIN 
    film f ON fc.film_id = f.film_id
JOIN 
    inventory i ON f.film_id = i.film_id
JOIN 
    rental r ON i.inventory_id = r.inventory_id
JOIN 
    payment p ON r.rental_id = p.rental_id
WHERE 
    EXTRACT(QUARTER FROM r.rental_date) = 1 
    AND EXTRACT(YEAR FROM r.rental_date) = 2017
GROUP BY 
    c.name,
    p.amount 
HAVING 
	sum(p.amount) > 0
	
	
   drop view sales_revenue_by_category_qtr;
   select * from sales_revenue_by_category_qtr;

--Create Query Language Function - get_sales_revenue_by_category_qtr:
   
CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(current_quarter INT)
RETURNS TABLE(category_name VARCHAR, total_sales_revenue DECIMAL) AS $$
BEGIN
    RETURN QUERY
    SELECT 
        c.name AS category_name,
        SUM(p.amount) AS total_sales_revenue
    FROM 
        category c
    JOIN 
        film_category fc ON c.category_id = fc.category_id
    JOIN 
        film f ON fc.film_id = f.film_id
    JOIN 
        inventory i ON f.film_id = i.film_id
    JOIN 
        rental r ON i.inventory_id = r.inventory_id
    JOIN 
        payment p ON r.rental_id = p.rental_id
    WHERE 
         EXTRACT(QUARTER FROM r.rental_date) = current_quarter 
    AND EXTRACT(YEAR FROM r.rental_date) = 2017
    GROUP BY 
        c.name,
        p.amount 
    HAVING 
        sum(p.amount) > 0;
END;
$$ LANGUAGE plpgsql;



--Create Procedure Language Function - new_movie:

CREATE OR REPLACE FUNCTION new_movie(movie_title VARCHAR)
RETURNS VOID AS $$
DECLARE
    new_film_id INT;
    lang_exists INT;
BEGIN
    -- Generate a new unique film ID
    SELECT MAX(film_id) + 1 INTO new_film_id FROM film;
   
    -- Check if the language 'Klingon' exists in the language table
    SELECT COUNT(*) INTO lang_exists FROM "language" WHERE name = 'Klingon';
    
    -- If 'Klingon' does not exist, insert it into the language table
    IF lang_exists = 0 THEN
        INSERT INTO "language" (name) VALUES ('Klingon');
    END IF;

    -- Check if the film with the given title already exists
    IF NOT EXISTS (SELECT 1 FROM film WHERE title = movie_title) THEN
        -- Insert a new film with default values
        INSERT INTO film (film_id, title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
        VALUES (new_film_id, movie_title, 4.99, 3, 19.99, EXTRACT(YEAR FROM CURRENT_DATE), 
            (SELECT language_id FROM "language" WHERE name = 'Klingon'));
    END IF;
END;
$$ LANGUAGE plpgsql;



select * from film;

